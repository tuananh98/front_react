//Dưới bản webpack 5.0.3 dùng const merge = require('webpack-merge');
//Từ bản 5.0.3 trở lên dùng:
const { merge } = require('webpack-merge');
const commonConfig = require('./webpack.config')

module.exports = merge(commonConfig, {
  //Khi webpack biên dịch và đóng gói mã nguồn của bạn, việc theo dõi lỗi hay cảnh báo có thể trở nên khó khăn. 
  //Ví dụ: nếu bạn có ba file code a.js b.js và c.js ghép thành một file ouput bundle.js, một trong các file nguồn có lỗi,
  //chỗ thông báo lỗi sẽ chỉ đến bundle.js. Điều này không phải lúc nào cũng hữu ích vì bạn có thể muốn biết chính xác lỗi đến từ đâu, dòng bao nhiêu, file nào bị lỗi.
  
  //Chỉ định mode là development, tốt nhất khi chúng ta đang trong quá trình viết code nên chọn chế độ mode: development để dễ dàng debug, theo dõi lỗi một cách dễ dàng.
  //Khi thêm option này chúng ta cũng có kết quả tương tự như dùng inline source map
  mode: 'development',
  //ánh xạ lại code ban đầu và cung cấp lỗi ở file nào và ở vị trí nào
  devtool: 'inline-source-map',
  devServer: {
    //Chỉ định nội dung ứng dụng sẽ nằm trong static
    static: './bundle',
    port:8069
  },
  module: {
    rules: [
      {
        //regular expression của tên các file áp dụng vơi rule
        test: /\.css$/,
        //style-loader inject CSS vào DOM
        use: ['style-loader', 'css-loader']
      },
    ]
  }
})
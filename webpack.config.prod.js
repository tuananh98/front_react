//Dưới bản webpack 5.0.3 dùng const merge = require('webpack-merge');
//Từ bản 5.0.3 trở lên dùng:
const { merge } = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const commonConfig = require('./webpack.config')

module.exports = merge(commonConfig, {
  mode: 'production',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ],
      },
    ],
  },
  plugins: [
    //Trong production, khi reload lại trang sẽ gặp trường hợp css không được tải cùng lúc vào trang trong một khoảng thời gian ngắn
    //Để khắc phục điều trên, dùng MiniCssExtractPlugin
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
  ],
  optimization: {
    minimizer: [
      //Được sử dụng để làm nhỏ lại file CSS trong quá trình build
      new OptimizeCssAssetsPlugin({
        cssProcessorOptions: {
          map: {
            inline: false,
            annotation: true,
          },
        },
      }),
      //Được sử dụng để thông báo làm nhỏ lại file JS trong khi optimize trong quá trình build
      new TerserPlugin({
        // sử dụng multil-process chạy song song để cải tiến tốc độ build
        // mặc định số tiến trình chạy đồng thời: os.cpus().length - 1
        parallel: true
      }),
    ],
  },
})
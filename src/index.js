import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { AUTHOR, NODE_ENV } from './constants/environment'

ReactDOM.render(
    <h1>Hello {AUTHOR}, đây là môi trường {NODE_ENV == 'production'? 'PRODUCTION': 'DEVELOPMENT'}! </h1>,
  document.getElementById('root')
);
const AUTHOR = process.env.AUTHOR;
const NODE_ENV = process.env.NODE_ENV;

export {
	AUTHOR,
	NODE_ENV
};
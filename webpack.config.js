const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
  entry: path.resolve(__dirname, './src/index.js'),
  output: {
    path: path.resolve(__dirname, './bundle'),
    filename: '[name].[contenthash].js',
  },
  plugins: [
    //Clean folder output trước mỗi lần bundle
    new CleanWebpackPlugin(),
    //tạo 1 html khác có nội dung giống file html trong template, đặt trong thư mục output và tự động add script trỏ đến file output mỗi khi output thay đổi
    new HtmlWebpackPlugin({
      filename: 'index.html',
      inject: true,
      template: path.join(__dirname, "./public/index.html"),
    }),
    new Dotenv(),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        //Loại trừ cái gì
        exclude: /[\\/]node_modules[\\/]/,
        use: {
          loader: 'babel-loader',
        },
      },
    ]
  },
};